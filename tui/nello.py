#!/usr/bin/env python3

import os
from pathlib import Path
import click
from click_repl import register_repl
import subprocess
import tempfile
from Pyro5.api import locate_ns, Proxy
import Pyro5.errors
import sys
from datetime import datetime
import webbrowser

sys.excepthook = Pyro5.errors.excepthook


@click.group()
@click.pass_context
def nello_tui(ctx):
    ctx.ensure_object(dict)
    #backend = Nello(data_path=Path("data").absolute(), config_file=Path(".nello-config.yaml"))
    servers = []
    try:
        with locate_ns() as ns:
            for nello_server, nello_uri in ns.list(prefix="nello.server.").items():
                print(f"found server: {nello_server}")
                servers.append(Proxy(nello_uri))
    except Exception as e:
        raise Exception("Name server not found, closing")
    if not servers:
        raise Exception("No Servers found, closing")
    backend = servers[0]
    ctx.obj['backend'] = backend

@nello_tui.group()
@click.pass_context
@click.argument("task", type=click.Path(dir_okay=False))
def create(ctx, task):
    ctx.obj["task"] = task


@create.command()
@click.pass_context
@click.argument("template", type=click.Path(dir_okay=False))
def from_template(ctx, template):
    ctx.obj['backend'].create_task(ctx.obj['task'], template)

@create.command()
@click.pass_context
def from_scratch(ctx):
    ctx.obj['backend'].create_task(out_path=ctx.obj['task'])
    click.echo("Created from scratch")

@nello_tui.command()
@click.pass_context
@click.argument("task", type=click.Path(dir_okay=False))
def edit(ctx, task):
    try:
        cfg = ctx.obj['backend'].config
        if cfg is None:
            raise Exception("Edit command requires having a config file")
        elif "editor" not in cfg.keys():
            raise Exception("Edit command requires setting an editor in config file")
        contents = ctx.obj['backend'].get_task_content(task)
        # Can't use normal block with r/w mode because delete behavior
        temp_f = tempfile.NamedTemporaryFile(mode='w+', suffix=".md", delete=False)
        temp_f.write(contents)
        temp_f.close()
        subprocess.call([cfg['editor'], temp_f.name])
        temp_f = open(temp_f.name, 'r')
        to_write_contents = temp_f.read()
        temp_f.close()
        ctx.obj['backend'].set_task_content(task, to_write_contents)
        os.unlink(temp_f.name)
    except Exception as e:
        print(e)

@nello_tui.command()
@click.pass_context
@click.argument("task", type=click.Path(dir_okay=False))
def block(ctx, task):
    url = ctx.obj['backend'].time_block(task)
    webbrowser.open_new_tab(url)


@nello_tui.command()
@click.pass_context
@click.argument("task", type=click.Path(dir_okay=False))
def delete(ctx, task):
    ctx.obj['backend'].delete_task(task)

@nello_tui.command()
@click.pass_context
def list_tasks(ctx):
    task_list = ctx.obj['backend'].list_tasks()
    for t in task_list:
        click.echo(t)

@nello_tui.command()
@click.pass_context
def list_templates(ctx):
    template_list = ctx.obj['backend'].list_templates()
    for t in template_list:
        click.echo(t)


if __name__ == "__main__":
    register_repl(nello_tui)
    nello_tui()
