import unittest
from nello_server import Nello

class NelloServerTest(unittest.TestCase):

  def setUp(self):
    self.n = Nello(data_path="../data", config_file="../.nello-config.yaml")

  def test_create(self):
    self.n.delete_task("to_schedule/random/test.md")
    self.n.create_task("random/test.md")

  def test_create_from_template(self):
    self.n.delete_task("to_schedule/random/test.md")
    self.n.create_task("random/test.md", template_path="car/random/fix_mudflaps.md")


if __name__ == '__main__':
  unittest.main()