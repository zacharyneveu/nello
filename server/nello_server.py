from pathlib import Path
from re import S, template
import webbrowser
from jinja2 import Environment, FileSystemLoader
from datetime import datetime
import frontmatter as fm # type: ignore
import schedule # type: ignore
import time
from typing import Dict, Callable, Union
import os
import yaml
from Pyro5.api import expose, Daemon, locate_ns

@expose
class Nello:

    class GCalTimeBlocker:
        url = "https://calendar.google.com/calendar/u/0/r/eventedit?state="
        def __init__(self):
            pass
        def __call__(self, title: str) -> str:
            return self.url
    class Callbacks:
        """
        Simple struct to mark possible callbacks
        """

        def __init__(
            self,
            on_new_instance: Callable[[Path], None] = None,
            on_repeat_task_scheduled: Callable[[Path], None] = None,
            on_repeat_task_stopped: Callable[[Path], None] = None,
        ) -> None:
            self.on_new_instance = on_new_instance
            self.on_repeat_task_scheduled = on_repeat_task_scheduled
            self.on_repeat_task_stopped = on_repeat_task_stopped

    class SpecialPaths:
        """
        Special paths that Nello relies on
        data - base path to data dir
        templates - directory with all templates
        tasks - directory with all tasks
        to_schedule - directory with links to tasks which haven't been scheduled
        """
        def __init__(self, data_path : Union[str, Path]):
            self.data = Path(data_path)
            if not self.data.exists():
                raise Exception(f"Data path {self.data} does not exist")
            self.templates = self.data/"templates"
            if not (self.templates/"special/all_meta.md").exists():
                raise Exception(f"{self.templates}/special/all_meta.md does not exist")
            self.to_schedule = self.data/"needs_time_block"
            self.tasks = self.data/"tasks"

    def __init__(self, data_path : str = Path("./data"), callbacks : Callbacks=Callbacks(), config_file : Union[str, None] = None):
        """
        Create a Nello instance
        Args:
            data_path: base dir of data for this instance
            callbacks: optional callbacks list
        """
        self.paths = self.SpecialPaths(data_path)
        # keep track of callbacks
        self.cbs = callbacks
        if config_file is not None:
            with open(config_file, 'r') as config:
                self._config = yaml.safe_load(config)
        else:
            self._config = None

        # Load Jinja templates at start
        l = FileSystemLoader(self.paths.templates, followlinks=True)
        self.jenv = Environment(loader=l, trim_blocks=True)

        # make sure we don't double schedule repeating jobs
        self.repeat_dict : Dict[Path, bool] = {}
        self.schedule_repeat_tasks()
        
        self.time_blocker = self.GCalTimeBlocker()

    @property
    def config(self):
        """
        Config dictionary with details of how this server is set up
        """
        return self._config

    def list_tasks(self):
        paths_list = self.paths.to_schedule.rglob("*.md")
        return [str(p.relative_to(self.paths.to_schedule)) for p in paths_list]

    def list_templates(self):
        paths_list = self.paths.templates.rglob("*.md")
        return [str(p.relative_to(self.paths.templates)) for p in paths_list]

    def schedule_repeat_tasks(self):
        """
        Recursively search all templates and schedule recurring tasks
        """
        for template in self.paths.templates.rglob("*.md"):
            try:
                self.schedule_repeat_task(str(template.relative_to(self.paths.templates)))
            except Exception as e:
                print(e)

    def schedule_repeat_task(self, template : str):
        """
        Schedules a single repeating task
        Args:
            template (Path) - the template to schedule
        """
        # Need to populate template to have proper YAML to parse
        filled = self._populate_template(template)
        meta = fm.parse(filled)
        if "repeat" in meta[0].keys():
            # Hacky way to read something like "every(3).minutes" from task and execute it on that schedule
            # not suuper dangerous as we're providing the target not letting them provide it?
            schedule_txt = "schedule." + meta[0]["repeat"]
            try:
                if str(template) not in self.repeat_dict.keys():
                    schedule_job = eval(schedule_txt)
                    schedule_job.do(self.create_task, template)
                    self.repeat_dict[template] = True
                else:
                    print(f"Task {template} already scheduled, skipping")
            except:
                raise Exception(f"Schedule `{schedule_txt}` invalid")

            if self.cbs.on_repeat_task_scheduled:
                self.cbs.on_repeat_task_scheduled(template)

    def stop_repeat_task(self, template : str):
        """
        Stops a repeating task from running in the future
        """
        if template in self.repeat_dict.keys():
            self.repeat_dict.pop(template)
        if self.cbs.on_repeat_task_stopped:
            self.cbs.on_repeat_task_stopped(template)

    def stop_repeat_tasks(self):
        tasks = self.repeat_dict.copy()
        self.repeat_dict.clear()
        if self.cbs.on_repeat_task_stopped:
            for task in tasks.keys():
                self.cbs.on_repeat_task_stopped(task)



    def _populate_template(self, template_path : str) -> str:
        """
        Use Jinja2 to populate a template and get back the full text of an instance
        """
        try:
            template = self.jenv.get_template(template_path)
        except Exception as e:
            raise Exception(f"Template not found {template_path} \nchoose from: {self.jenv.list_templates()}")

        template.globals["now"] = datetime.utcnow()
        return template.render(date="today")

    def create_task(self, out_path : str, template_path : Union[str, None] = None, ) -> None:
        """
        Create an instance file from a template file, populating variables in the process
        Args:
            template_path - relative path to template from templates folder
            out_path - relative path to created task from tasks folder
        """
        task_out_path = self.paths.tasks/out_path
        if template_path is not None:
            out_str = self._populate_template(template_path)
        else:
            out_str = self._populate_template("special/all_meta.md")

        if not task_out_path.parent.exists():
            task_out_path.parent.mkdir(parents=True)

        if task_out_path.exists():
            raise Exception(f"Task {out_path} already exists")

        with open(task_out_path, "w+") as of:
            of.write(out_str)

        symlink_out_path = self.paths.to_schedule/out_path
        if not symlink_out_path.parent.exists():
            symlink_out_path.parent.mkdir(parents=True)
        os.symlink(task_out_path, symlink_out_path)

        if self.cbs.on_new_instance:
            self.cbs.on_new_instance(out_path)

    def delete_task(self, task: str) -> None:
        if (self.paths.tasks/task).exists():
            os.remove(self.paths.tasks/task)

    def get_task_content(self, task: str) -> str:
        """
        Get the contents of a task markdown file
        """
        ret = ""
        if (self.paths.tasks/task).exists():
            with open(self.paths.tasks/task, 'r') as f:
                ret = f.read()
        else:
            raise Exception("Task does not exist to get content")
        return ret

    def set_task_content(self, task: str, content: str) -> None:
        """
        Set the content of a task
        Note: this should be used in conjunction with `get_task_content()` to edit tasks
              use `create_task()` instead to create tasks.
        """
        with open(self.paths.tasks/task, 'w+') as f:
            print(f"writing {self.paths.tasks/task}")
            f.write(content)

    def run_repeat_tasks(self, min_sched_s : int =1) -> None:
        """
        Run any repeating tasks that are due
        """
        while True:
            try:
                schedule.run_pending()
                time.sleep(min_sched_s)
            except KeyboardInterrupt:
                break
            except Exception as e:
                print(e)

    def time_block(self, task: str) -> str:
        return self.time_blocker(task)


if __name__ == '__main__':
    nello_01 = Nello(data_path = Path("../data"), config_file=Path("../.nello-config.yaml"))
    with Daemon() as daemon:
        nello_01_uri = daemon.register(nello_01)
        with locate_ns() as ns:
            ns.register("nello.server.first", nello_01_uri)
        print("Nello online.")
        daemon.requestLoop()
