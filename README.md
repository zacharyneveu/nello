# Reasons
I really like Trello and have been using it for quite awhile. In particular, Butler is a fantastic tool for automation and enables some really powerful workflows. I recently started using Notion, and was struck by their simple concept of markdown pages with metadata as the "core object" to organize. Notion is missing automation though, and Trello lacks much of the flexibility of Notion. 

Both of these programs suffer from licensing issues so I can't trust them for long term use or logging. For example, if I want to track maintenance on my home over the next 10 years, then create a report for the next owner, I'm not confident Notion/Trello will even be around by then. And maybe they will be, but what if I don't have the money to pay for a subscription at some point? All of a sudden features like bulk data export, robust version history, unlimited automation actions and more disappear. 

These issues prompt me to want to build a simple task management program, based around some basic principles that current corporate PM software have not embraced.
# Principles
+ Longevity
  + Use standard formats for everything
  + Use common languages and common dependencies (as few as feasible) for all programs
+ Composability
  + Favor open, standard APIs between components
  + Interface with existing bits rather than recreating them where possible
+ Data & Version Safety
  + Use strong version control to track changes
  + Support multiple servers and backups

# Goals
## Usage
+ Support repeating and one-off tasks
+ Support arbitrary automation via actual code
+ Help manage the transition from tasks to time blocks
+ Support logs/reports of completed tasks

## Technical
+ Store all data in common formats in local storage for easy export
  + Markdown for rich "page"/"card" replacement
  + JSON for metadata (schedule, tags, etc.)
  + Standard Unix filesystem for organization
+ Use Git under the hood for distributed instances and robust version control
+ Provide public API between backend and display for app/web compatibility
+ Provide text-only interface
