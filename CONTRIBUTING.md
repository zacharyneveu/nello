# Design Decisions
+ Server API will only include standard types
  + This is mostly for the sake of Pyro, but also to simplify client dependencies more generally

# Unsolved Problems
### Behavior if program stopped and misses scheduled task
For longer scheduled tasks (every year or something) it is super likely that the server will go down between events. Scheduling using something like `every().month.on(12)` instead of `every(4).weeks` will help make sure a short pause mid-month doesn't totally mess things up. It would still be nice to persist the data but need to figure out what the desired behavior is if a scheduled event is missed because server down. Would make most sense IMO to schedule the event still just late I think?
